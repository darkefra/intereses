﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        static double calcular(double a, double b, double c)
        {
            double total, total1 = 0;
            double t = b / 100;
            for (int d = 0; d < c; d++)
            {
                total = a * t;
                total1 = total1 + total;
            }
            return total1;
        }
        public void Capital()
        {
            double capital;
            double interes;
            double mes;

            capital = Convert.ToDouble(box1.Text);
            if (capital < 500)
            {
                MessageBox.Show(capital + " capital erroneo", "Adevertencia", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                interes = Convert.ToDouble(box2.Text);
                if(interes > 8 || interes < 1)
                {
                    MessageBox.Show(interes + " Taza de interes erronea", "Adevertencia", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    mes = Convert.ToDouble(box3.Text);
                    if(mes > 12 || mes < 1)
                    {
                        MessageBox.Show(mes + " Numeros de mes  erroneo", "Adevertencia", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    else
                    {
                        double x = calcular(capital, interes, mes);
                        MessageBox.Show(" su interes general en " + mes + "meses es" + x);
                        double capt = x + capital;
                    MessageBox.Show("Su capital total es" + capt);
                    }

                }
            }
        }

        private void BTN1_Click(object sender, EventArgs e)
        {
            Capital();
        }
    }
}
